INSERT INTO public.role (id, rolename) VALUES (1, 'ROLE_ADMIN');
INSERT INTO public.role (id, rolename) VALUES (2, 'ROLE_CUSTOMER');


INSERT INTO usertable (id, enable, password, username, first_name, last_name, middle_name, date_birthday) VALUES (1, true, '$2a$10$lFid6jorM6uT8U/.Za1FB.5hV6AIZsk70xZ/oef49jYttSQ3PaCLG', 'admin', 'Dmitry', 'Mirutin', 'Vadimovich', '2001-02-26');
INSERT INTO usertable (id, enable, password, username, first_name, last_name, middle_name, date_birthday) VALUES (2, true, '$2a$10$lFid6jorM6uT8U/.Za1FB.5hV6AIZsk70xZ/oef49jYttSQ3PaCLG', 'customer', 'Alexandr', 'Gudkov', 'Vladimirovich', '1983-02-24');
INSERT INTO usertable (id, enable, password, username, first_name, last_name, middle_name, date_birthday) VALUES (3, true, '$2a$10$lFid6jorM6uT8U/.Za1FB.5hV6AIZsk70xZ/oef49jYttSQ3PaCLG', 'John', 'SDF', 'fdsfds', 'fdsdsf', '1983-02-24');
INSERT INTO usertable (id, enable, password, username, first_name, last_name, middle_name, date_birthday) VALUES (4, false, '$2a$10$lFid6jorM6uT8U/.Za1FB.5hV6AIZsk70xZ/oef49jYttSQ3PaCLG', 'Idol', 'John', 'Mitov', 'Userfsds', '1983-02-24');


INSERT INTO public.usersroles (userid, roleid) VALUES (1, 1);
INSERT INTO public.usersroles (userid, roleid) VALUES (2, 2);


insert into public.product (id, name, price) values(1,'Пиво Esse',50.55);
insert into public.product (id,name, price) values(2,'Огурец',120.23);
insert into public.product (id,name, price) values(3,'Банан',63.55);
insert into public.product (id,name, price) values(4,'Киви',183.55);
insert into public.product (id,name, price) values(5,'Шампанское Crystal',1500);
insert into public.product (id,name, price) values(6,'Вино Кагор',500);
insert into public.product (id,name, price) values(7,'Snickers',60);
insert into public.product (id,name, price) values(8,'Twix',70);
insert into public.product (id,name, price) values(9,'Говяжьи кости',75);
insert into public.product (id,name, price) values(10,'Карпаччо',233);
insert into public.product (id,name, price) values(11,'Люля кебаб',270);
insert into public.product (id,name, price) values(12,'Бекон из мраморной говядины',150);
insert into public.product (id,name, price) values(13,'Рыбные кости',180);
insert into public.product (id,name, price) values(14,'Скумбрия',72);
insert into public.product (id,name, price) values(15,'Рыба пикша тушка',245.55);
insert into public.product (id,name, price) values(16,'Сельдь русское море',293);
insert into public.product (id,name, price) values(17,'Чай черный',19);
insert into public.product (id,name, price) values(18,'Чай Лисма',52.55);
insert into public.product (id,name, price) values(19,'Кофе',10.55);
insert into public.product (id,name, price) values(20,'Капучино',15.55);
insert into public.product (id,name, price) values(21,'Гречка',58.55);
insert into public.product (id,name, price) values(22,'Рис',48.55);
insert into public.product (id,name, price) values(23,'Манная',48.55);
insert into public.product (id, name, price) values(24,'Пиво Bud',55);
insert into public.product (id, name, price) values(25,'Помидор',167);


insert into public.category(id, name) values(1, 'Алкоголь');
insert into public.category(id, name) values(2, 'Овощи');
insert into public.category(id, name) values(3, 'Фрукты');
insert into public.category(id, name) values(4, 'Шоколадки');
insert into public.category(id, name) values(5, 'Мясо');
insert into public.category(id, name) values(6, 'Рыба');
insert into public.category(id, name) values(7, 'Чай, кофе');
insert into public.category(id, name) values(8, 'Бакалея, крупы');


insert into public.catalogitem(categoryid, productid) values (1,1);
insert into public.catalogitem(categoryid, productid) values (1,5);
insert into public.catalogitem(categoryid, productid) values (1,6);
insert into public.catalogitem(categoryid, productid) values (1,24);
insert into public.catalogitem(categoryid, productid) values (2,25);
insert into public.catalogitem(categoryid, productid) values (2,2);
insert into public.catalogitem(categoryid, productid) values (3,3);
insert into public.catalogitem(categoryid, productid) values (3,4);
insert into public.catalogitem(categoryid, productid) values (4,7);
insert into public.catalogitem(categoryid, productid) values (4,8);
insert into public.catalogitem(categoryid, productid) values (5,9);
insert into public.catalogitem(categoryid, productid) values (5,10);
insert into public.catalogitem(categoryid, productid) values (5,11);
insert into public.catalogitem(categoryid, productid) values (5,12);
insert into public.catalogitem(categoryid, productid) values (6,13);
insert into public.catalogitem(categoryid, productid) values (6,14);
insert into public.catalogitem(categoryid, productid) values (6,15);
insert into public.catalogitem(categoryid, productid) values (6,16);
insert into public.catalogitem(categoryid, productid) values (7,17);
insert into public.catalogitem(categoryid, productid) values (7,18);
insert into public.catalogitem(categoryid, productid) values (7,19);
insert into public.catalogitem(categoryid, productid) values (7,20);
insert into public.catalogitem(categoryid, productid) values (8,21);
insert into public.catalogitem(categoryid, productid) values (8,22);
insert into public.catalogitem(categoryid, productid) values (8,23);


