package ru.onlineshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.onlineshop.entity.UsertableEntity;

@Repository
public interface UsertableRepository extends JpaRepository<UsertableEntity, Long> {
    UsertableEntity findByUsername(String username);
}
