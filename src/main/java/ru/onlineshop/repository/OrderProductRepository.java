package ru.onlineshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.onlineshop.entity.OrderEntity;
import ru.onlineshop.entity.OrderProductEntity;

import java.util.List;

@Repository
public interface OrderProductRepository
        extends JpaRepository<OrderProductEntity, Long> {
    List<OrderProductEntity> findByOrder(OrderEntity order);
}
