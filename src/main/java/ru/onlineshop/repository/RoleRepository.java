package ru.onlineshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.onlineshop.entity.RoleEntity;

public interface RoleRepository extends JpaRepository<RoleEntity,Long> {
}
