package ru.onlineshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.onlineshop.entity.CategoryEntity;

@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {

    //todo мб здесь лучше получать Optional
    CategoryEntity findCategoryEntityByName(String name);
}
