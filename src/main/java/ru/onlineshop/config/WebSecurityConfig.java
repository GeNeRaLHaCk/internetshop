package ru.onlineshop.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.onlineshop.service.UserService;


//@EnableWebSecurity(debug = true)
//@EnableConfigurationProperties
//@EnableGlobalMethodSecurity(securedEnabled = true) //это в случае если всё
// таки пробрались через защиту, чтобы защитить МЕТОДЫ
// можем обозначить их аннотацией @Secured("Имя роли, права")
@Configuration
@ComponentScan("ru.onlineshop")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
    //websecurconfigadapter позволяет настрoить всю систему security
    //под свои нужды
    //полностью отключаем автоконфигурацию приложения по умолчанию
    private final UserService userService;
    private final BCryptPasswordEncoder bcrypt;
    @Autowired
    public WebSecurityConfig(UserService userService,
                             BCryptPasswordEncoder bcrypt) {
        this.userService = userService;
        this.bcrypt = bcrypt;
    }
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean()
            throws Exception {
        return super.authenticationManagerBean();
    }

    @Autowired
    protected void configureAuthentication(AuthenticationManagerBuilder builder)
            throws Exception {
        System.out.println("AUTHMANAGERBUILDER");
        builder.userDetailsService(this.userService)
                .passwordEncoder(bcrypt);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity)
            throws Exception {
        httpSecurity
                .csrf().disable()//timeleaf по умолчанию включает это, от инжекций
                .cors().disable()
                .formLogin().disable()
                .httpBasic().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,
                        "/**",
                        "/v3/**",
                        "/*.html",
                        "/favicon.ico",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js"
                ).permitAll()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .antMatchers("/user/**").permitAll()
                .antMatchers("/login/**").permitAll()
                .antMatchers("/catalog/**").permitAll()
                .antMatchers("/register/**").permitAll()
                //по умолчанию Teacher эт admin
                .anyRequest().authenticated()
                .and()
                .logout().disable();
    }

/*    @Bean
    public JdbcUserDetailsManager users(DataSource dataSource){
        //создать базовых юзеров сразу
        UserDetails user = User.builder()
                .username("user")
                .password("{bcrypt}")
                .roles("USER")
                .build();
        UserDetails admin = User.builder()
                .username("user")
                .password("{bcrypt}")
                .roles("ADMIN")
                .build();
        JdbcUserDetailsManager jdbcUserDetailsManager =
                new JdbcUserDetailsManager(dataSource);
        if(jdbcUserDetailsManager.userExists(user.getUsername()))
            jdbcUserDetailsManager.deleteUser(user.getUsername());
        if(jdbcUserDetailsManager.userExists(admin.getUsername()))
            jdbcUserDetailsManager.deleteUser(admin.getUsername());
        jdbcUserDetailsManager.createUser(user);
        jdbcUserDetailsManager.createUser(admin);
        return jdbcUserDetailsManager;
    }*/

/*    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider(){
        //отдаём ему логин пароль, а он должен сказать существует такой юзер или нет
        DaoAuthenticationProvider authenticationProvider =
        new DaoAuthenticationProvider();
        authenticationProvider.setPasswordEncoder(encoder());
        authenticationProvider.setUserDetailsService(userService);
        return authenticationProvider;
    }*/
/*    @Bean
    public FilterRegistrationBean disableAutoRegistration(TokenAuthenticationFilter filter) {
        FilterRegistrationBean registration = new FilterRegistrationBean(filter);
        registration.setEnabled(false);//в противном случае
        // фильтр регистрируется дважды(в конфигурации и
        // путем автоматического обнаружения
        return registration;
    }*/
}