package ru.onlineshop.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "address")
public class AddressEntity implements Serializable {
    @Id
    @GenericGenerator(name="kaugen" , strategy="increment")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator="kaugen")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "flat")
    private String flat;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressEntity that = (AddressEntity) o;
        return id.equals(that.id) && name.equals(that.name) && flat.equals(that.flat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, flat);
    }
}
