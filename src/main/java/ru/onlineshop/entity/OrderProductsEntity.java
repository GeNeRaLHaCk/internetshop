/*
package ru.onlineshop.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "order_product")
@SequenceGenerator(name = "orderproduct_gen",
        sequenceName = "orderproduct_id_seq",
        allocationSize = 1)
public class OrderProductsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="orderproduct_gen")
    @Column(nullable = false)
    private Long id;
    @ManyToOne
    @JoinColumn(name="orderid", nullable=false,
            foreignKey = @ForeignKey(
                    name = "orderproduct_order_id_fk"))//Как назовётся
    // колонка в таблице(name joincolumna)
    private OrderEntity order;
}
*/
