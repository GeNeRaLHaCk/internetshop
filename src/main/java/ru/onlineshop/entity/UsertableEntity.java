package ru.onlineshop.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "usertable")
@Data
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "usertable_gen", sequenceName = "usertable_id_seq")
public class UsertableEntity implements Serializable {
    @Id
    @GenericGenerator(name="kaugen" , strategy="increment")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="kaugen")
    private Long id;
    @Column(name = "password", length = 100)
    private String password;
    @Column(name = "username")
    private String username;
    @Column(name = "first_name", length = 50, nullable = false)
    private String first_name;
    @Column(name = "last_name", length = 50, nullable = false)
    private String last_name;
    @Column(name = "middle_name",length = 50, nullable = false)
    private String middle_name;
    @Column(name = "date_birthday", nullable = false)
    private Date date_birthday;
    @Column(name = "enable")
    private boolean enable;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "usersroles",
            joinColumns = {
                    @JoinColumn(name="userid",
                            foreignKey = @ForeignKey(name = "usersroles_user_userid_fk"))
            },
            inverseJoinColumns = @JoinColumn(name = "roleid"))
    @Column(nullable = false)
    private Collection<RoleEntity> roles;
}

