package ru.onlineshop.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "product")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class ProductEntity implements Serializable {
    @Id
    @GenericGenerator(name="kaugen" , strategy="increment")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator="kaugen")
    @Column(nullable = false)
    private Long productid;
    @Column(name = "name", length = 50)
    private String name;
    @Column(name = "price", nullable = false)
    private float price;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "catalogitem",
            joinColumns = {
                    @JoinColumn(name="productid",
                            foreignKey =
                            @ForeignKey(name = "catalogitem_product_id_fk"))
            },
            inverseJoinColumns = @JoinColumn(name = "categoryid"))
    private List<CategoryEntity> categories;

/*    @JsonIgnoreProperties
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product",
            cascade = CascadeType.ALL)
    private Set<OrderProductEntity> orderProductEntities;*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductEntity that = (ProductEntity) o;
        return Float.compare(that.price, price) == 0 && productid.equals(that.productid) && name.equals(that.name) && categories.equals(that.categories) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(productid, name, price, categories);
    }
}

