package ru.onlineshop.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "orderproduct")
@SequenceGenerator(name = "order_product_gen",
        sequenceName = "order_product_id_seq",
        allocationSize = 1)
public class OrderProductEntity {
    @Id
    @GenericGenerator(name="kaugen" , strategy="increment")
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator="kaugen")
    //можно сделать columndefinition="serial"
    @Column(nullable = false)
    private Long order_product_id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "orderid", nullable = false,
            foreignKey = @ForeignKey(name = "orderproduct_order_id_fk"),
    referencedColumnName = "orderid")
    private OrderEntity order;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "productid", nullable = false,
            foreignKey = @ForeignKey(name = "orderproduct_product_id_fk"),
            referencedColumnName = "productid")
    private ProductEntity product;

    private Integer quantity;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderProductEntity that = (OrderProductEntity) o;
        return order_product_id.equals(that.order_product_id) && order.equals(that.order) && product.equals(that.product) && quantity.equals(that.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(order_product_id, order, product, quantity);
    }
}
