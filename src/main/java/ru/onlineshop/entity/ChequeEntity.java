package ru.onlineshop.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cheque")
@SequenceGenerator(name = "cheque_gen", sequenceName = "cheque_id_seq",
        allocationSize = 1)
public class ChequeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="cheque_gen")
    //можно сделать columndefinition="serial"
    @Column(nullable = false)
    private Long chequeid;

/*    @ManyToOne(targetEntity = OrderEntity.class)
    @JoinColumn(name="order_id", nullable=false,
            foreignKey = @ForeignKey(
                    name = "cheque_order_orderid_fk"))//Как назовётся*/
    @OneToOne(targetEntity = OrderEntity.class)
    @JoinColumn(name="order_id", nullable=false,
            foreignKey = @ForeignKey(
                    name = "cheque_order_orderid_fk"))//Как назовётся
    // колонка в таблице(name joincolumna)
    private OrderEntity order;

    @Column(name="summ")
    private float summ;
}
