package ru.onlineshop.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "role")
@SequenceGenerator(name = "role_gen", sequenceName = "usertable_id_seq")
public class RoleEntity implements GrantedAuthority, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="role_gen")
    private Long id;
    @Column(name = "rolename", length = 30)
    private String name;
    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinTable(
            name = "usersroles",
            joinColumns = {
                    @JoinColumn(name="roleid",
                            foreignKey = @ForeignKey(name = "usersroles_role_roleid_fk"))
            },
            inverseJoinColumns = @JoinColumn(name = "userid"))
    private List<UsertableEntity> users;
    public RoleEntity(){}
    public RoleEntity(Long id) {
        this.id = id;
    }

    //todo для чего такой конструктор нужен?
    public RoleEntity(String name) {
        this.name = name;
    }
    public RoleEntity(Long id, String name) {
        this.id = id;
        this.name = name;
    }
    @Override
    public String getAuthority() {
        return name;
    }

    @Override
    public String toString() {
        return "RoleEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", users=" + users +
                '}';
    }
}
