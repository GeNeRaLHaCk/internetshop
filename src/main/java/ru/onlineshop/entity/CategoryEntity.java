package ru.onlineshop.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "category")
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class)
@JsonIgnoreProperties({"@UUID"})
public class CategoryEntity implements Serializable {

    //todo все скучено, можно добавить пустые строки между переменными, воспринимается сложно)
    // еще лучше геттеры напрямую тут выводить, без использования ломбока,
    // чтобы добавить @Column(value = "name"). вдруг название столбца поменяется,
    // легче будет в одном месте поменять

    @Id
    @GenericGenerator(name="kaugen" , strategy="increment")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator="kaugen")
    private Long id;
    private String name;
    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinTable(
            name = "catalogitem",
            joinColumns = {
                    @JoinColumn(name="categoryid",
                            foreignKey = @ForeignKey(
                                    name = "catalogitem_category_id_fk"))
            },
            inverseJoinColumns = @JoinColumn(name = "productid"))
    private List<ProductEntity> products;
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryEntity that = (CategoryEntity) o;
        return id.equals(that.id) && name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
