package ru.onlineshop.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ordering")
@SequenceGenerator(name = "ordering_gen", sequenceName = "ordering_id_seq",
        allocationSize = 1)
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="ordering_gen")
    //можно сделать columndefinition="serial"
    @Column(nullable = false)
    private Long orderid;

    //todo лучше статусы через enum или справочники делать

    @Column(length = 20, nullable = false)
    private String paymentStatus;

    @Column(length = 20, nullable = false)
    private String deliveryStatus;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product",
        cascade = CascadeType.ALL)
    private List<ProductEntity> products;
    @ManyToOne
    @JoinColumn(name="user_id", nullable=false,
            foreignKey = @ForeignKey(
                    name = "order_user_id_fk"))//Как назовётся
    // колонка в таблице(name joincolumna)
    private UsertableEntity user;
    @Column(name = "cost")
    private float cost;
}
