package ru.onlineshop.security;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import ru.onlineshop.entity.RoleEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Component
@NoArgsConstructor
@Data
@Primary
@JsonIgnoreProperties({"hibern3ateLazyInitializer", "handler"})
//Principal (lichnostb polzovatelya)
public class Principal implements UserDetails, CredentialsContainer {
    private Long userid;
    private String username;
    private String password;
    private String first_name;
    private String last_name;
    private String middle_name;
    private Date date_of_birthday;
    private boolean enabled = false;
    private List<RoleEntity> roles = new ArrayList<>();

    public Principal(String username,
                     String password, String first_name,
                     String last_name, String middle_name,
                     Date date_of_birthday, boolean enabled,
                     List<RoleEntity> roles) {
        this.username = username;
        this.password = password;
        this.first_name = first_name;
        this.last_name = last_name;
        this.middle_name = middle_name;
        this.date_of_birthday = date_of_birthday;
        this.enabled = enabled;
        this.roles = roles;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setRoleCustomer(){
        RoleEntity role = new RoleEntity();
        role.setId(2L);//id userа
        role.setName("ROLE_CUSTOMER");
        System.out.println("SETROLEUSER " + role);
        roles.add(role);
    }

    @Override
    public void eraseCredentials() {
        this.password = null;
    }

    @Override
    public String toString() {
        return "Principal{" +
                "userid=" + userid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", middle_name='" + middle_name + '\'' +
                ", date_of_birthday=" + date_of_birthday +
                ", enabled=" + enabled +
                ", roles=" + roles +
                '}';
    }
    /*    private Collection<? extends GrantedAuthority>
    mapRolesToAuthorities(Collection<RoleEntity> role){
        return roles.stream().map(
                r -> new SimpleGrantedAuthority(r.getName()))
                .collect(Collectors.toList());
    }*/

}
