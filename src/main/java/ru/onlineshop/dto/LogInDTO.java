package ru.onlineshop.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class LogInDTO {

    //todo его можно заменить @NoArgsConstructor, раз ломбок уже используется
    public LogInDTO() {}
    @NotNull
    private String login;
    @NotNull
    private String password;
}
