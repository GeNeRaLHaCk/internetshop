package ru.onlineshop.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class RegisterDTO {
    public RegisterDTO() {}
    private String login;
    @NotNull
    private String password;
    @NotNull
    private String passwordConfirm;
    @NotNull
    @Size(min = 3, max = 50)
    private String first_name;
    @NotNull
    @Size(min = 2, max = 50)
    private String last_name;
    @NotNull
    @Size(min = 2, max = 50)
    private String middle_name;

    //todo лучше сразу получать в формате даты, например LocalDate. через v-date-picker, например
    @NotNull
    private String date_birthday;
}
