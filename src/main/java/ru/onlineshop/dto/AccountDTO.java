package ru.onlineshop.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import javax.validation.constraints.NotNull;

import java.util.Date;

@Data
@AllArgsConstructor
public class AccountDTO {
    public AccountDTO(){}
    public AccountDTO(String username){
        this.username = username;
    }
    public AccountDTO(Long id,
            @NotNull String username,
                      @NotNull String first_name,
                      @NotNull String last_name,
                      @NotNull String middle_name,
                      @NotNull Date date_of_birthday,
                      boolean enabled) {
        this.id = id;
        this.username = username;
        this.first_name = first_name;
        this.last_name = last_name;
        this.middle_name = middle_name;
        this.date_birthday = date_of_birthday;
        this.enable = enabled;
    }
    private Long id;

    //todo называть переменные надо lastName, userName.
    // https://www.oracle.com/java/technologies/javase/codeconventions-namingconventions.html

    @NotNull
    private String username;
    private String password;
    private String first_name;
    private String last_name;
    private String middle_name;
    private Date date_birthday;
    private boolean enable = false;
}
