package ru.onlineshop.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO {
    private Long orderid;
    private String paymentStatus;
    private String deliveryStatus;
    private AccountDTO user;
    private List<ProductDTO> products;
    private float cost;
}
