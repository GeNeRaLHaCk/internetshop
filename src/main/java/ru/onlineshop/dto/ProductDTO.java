package ru.onlineshop.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class,
        property="@UUID",
        scope=ProductDTO.class)
@JsonIgnoreProperties({"@UUID", "handler"})
public class ProductDTO {
    private Long productid;
    private String name;
    private float price;
    private List<CategoryDTO> categories;
    private Integer quantity;
}
