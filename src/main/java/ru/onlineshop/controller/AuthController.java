package ru.onlineshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.onlineshop.dto.AccountDTO;
import ru.onlineshop.dto.LogInDTO;
import ru.onlineshop.dto.RegisterDTO;
import ru.onlineshop.security.Principal;
import ru.onlineshop.service.UserAuthenticationService;

import javax.validation.Valid;

@RestController
@Validated
public class AuthController {

    private final UserAuthenticationService authentication;
    @Autowired
    public AuthController(UserAuthenticationService authentication) {
        this.authentication = authentication;
    }

    //todo так лучше не делать, вся логика должна быть в сервисе, в контроллере только принимаем
    @GetMapping("/login/{login}/{password}")
    @CrossOrigin
    public AccountDTO authorize(
            @PathVariable("login") String login,
            @PathVariable("password") String password)
    {
        LogInDTO logInDTO = new LogInDTO(login,password);
        System.out.println(login+"||||"+password);
        Authentication auth = authentication.authorize(logInDTO);
        Principal principal = (Principal) auth.getPrincipal();
        AccountDTO accountDTO = new AccountDTO(
                principal.getUserid(),
                principal.getUsername(),
                principal.getFirst_name(),
                principal.getLast_name(),
                principal.getMiddle_name(),
                principal.getDate_of_birthday(),
                principal.isEnabled());
        return accountDTO;
    }

    @PostMapping("/register")
    @CrossOrigin
    public AccountDTO register(@Valid @RequestBody
                                               RegisterDTO registerDTO)
            throws Exception {
            AccountDTO account =
                    authentication.registerCustomer(registerDTO);
            return account;
    }
    @GetMapping("/current")//заинжектить Principal текущий пользователь
    public Principal getCurrent(@AuthenticationPrincipal Principal user) {
        System.out.println("CURR: " + user);
        return user;
    }

    @GetMapping("/logout")
    public ResponseEntity logout(@AuthenticationPrincipal User user) {
        System.out.println("Logout: " + user);
        authentication.logout();
        return ResponseEntity.ok().body("Successfull logout");
    }
}
