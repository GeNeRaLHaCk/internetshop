package ru.onlineshop.controller;

import org.springframework.web.bind.annotation.*;
import ru.onlineshop.dto.CategoryDTO;
import ru.onlineshop.dto.ProductDTO;
import ru.onlineshop.service.CatalogService;

@RestController()
@RequestMapping("/catalog")
public class CatalogController {
    private final CatalogService catalogService;

    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @GetMapping("/item/{id}")
    public ProductDTO getProductById(@PathVariable("id") Long id){
        return catalogService.getProductById(id);
    }

    //todo лучше List возвращать
    @GetMapping("/list")
    @CrossOrigin
    public Iterable<ProductDTO> getAllProducts(){
        return catalogService.getAllProducts();
    }

    @GetMapping("/category/{id}/list")
    public Iterable<ProductDTO> getAllProductsByCategory(@PathVariable("id")
                                                         Long id){
        return null;//catalogService.getProductsByCategoryId(id);
    }

    @GetMapping("/categories")
    @CrossOrigin
    public Iterable<CategoryDTO> getAllCategories(){
        return catalogService.getAllCategories();
    }
}
