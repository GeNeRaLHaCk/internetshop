package ru.onlineshop.controller;

import org.springframework.web.bind.annotation.*;
import ru.onlineshop.dto.AccountDTO;
import ru.onlineshop.service.UserService;

@RestController
public class AdminController {
    private final UserService userService;

    public AdminController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users/list")
    @CrossOrigin
    public Iterable<AccountDTO> getUsersList() {
        return userService.getAllUsers();
    }
    @GetMapping("/user/item/{id}/deactivate")
    public AccountDTO deactivateUser(@PathVariable("id")
                                             Long id) {
        return userService.deactivateUser(id);
    }
    @GetMapping("/user/item/{id}/activate")
    public AccountDTO activateUser(@PathVariable("id")
                                             Long id) {
        return userService.activateUser(id);
    }
    @GetMapping("/user/item/{id}")
    public AccountDTO getUser(@PathVariable("id")
                                          Long id) {
        return userService.getUserById(id);
    }
    @DeleteMapping("/user/item/{id}")
    public AccountDTO deleteUser(@PathVariable("id")
                                      Long id) {
        return userService.deleteById(id);
    }
}
