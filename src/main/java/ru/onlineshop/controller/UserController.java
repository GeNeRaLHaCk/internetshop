package ru.onlineshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.onlineshop.dto.AccountDTO;
import ru.onlineshop.dto.OrderDTO;
import ru.onlineshop.service.OrderService;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    private final OrderService orderService;

    @Autowired
    private

    public UserController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/buy")
    @CrossOrigin
    public ResponseEntity placeAnOrder(@RequestBody OrderDTO order){
        orderService.makeOrder(order);
        return ResponseEntity.ok("Successfull");
    }
    @PostMapping("/orders")
    @CrossOrigin
    public Iterable<OrderDTO> getAllOrdersByUsername(
            @RequestBody AccountDTO account){
        List<OrderDTO> orders = orderService.getAllOrdersByUser(account);
        return orders;
    }
}
