package ru.onlineshop.controller;

import lombok.Data;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.onlineshop.exceptions.InvalidDateException;
import ru.onlineshop.exceptions.ProductsNotFoundException;
import ru.onlineshop.exceptions.UserAlreadyExistException;
import ru.onlineshop.exceptions.UserNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Exception e) {
        return ResponseEntity.badRequest().body(
                "{\r\n    \"error\": \"" + e.getMessage() + "\"\r\n}");
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors().stream()
                .map(f -> new FieldError(f.getObjectName(),
                        f.getField(), f.getCode()))
                .collect(Collectors.toList());
        return handleExceptionInternal(ex,
                "Method argument not valid, fieldErrors:"
                        + fieldErrors ,new HttpHeaders(),
                HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(UserAlreadyExistException.class)
    public ResponseEntity<MyException> handleUserAlreadyExistException() {
        return new ResponseEntity<>(
                new MyException("User with that username already exist"),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<MyException> handleUsernameNotFoundException() {
        return new ResponseEntity<>(
                new MyException("User with that username is not found"),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<MyException> handleUserNotFoundException() {
        return new ResponseEntity<>(
                new MyException("User with that id is not found"),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(InvalidDateException.class)
    public ResponseEntity<MyException> handleInvalidDataException() {
        return new ResponseEntity<>(
                new MyException("Invalid date"),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(ProductsNotFoundException.class)
    public ResponseEntity<MyException> handleProductsNotFound() {
        return new ResponseEntity<>(
                new MyException("Products not found"),
                HttpStatus.BAD_REQUEST);
    }
    @Data
    private static class MyException{
        private String error;

        public MyException(String message) {
            this.error = message;
        }
    }
}
