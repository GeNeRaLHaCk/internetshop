package ru.onlineshop.service;

import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import ru.onlineshop.dto.AccountDTO;
import ru.onlineshop.dto.CategoryDTO;
import ru.onlineshop.dto.OrderDTO;
import ru.onlineshop.dto.ProductDTO;
import ru.onlineshop.entity.OrderEntity;
import ru.onlineshop.entity.OrderProductEntity;
import ru.onlineshop.entity.ProductEntity;
import ru.onlineshop.entity.UsertableEntity;
import ru.onlineshop.repository.OrderProductRepository;
import ru.onlineshop.repository.OrderRepository;
import ru.onlineshop.repository.ProductRepository;
import ru.onlineshop.repository.UsertableRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OrderService {
    private final OrderRepository orderRepository;
    private final ModelMapper modelMapper;
    private final UsertableRepository usertableRepository;
    private final OrderProductRepository orderProductRepository;
    private final ProductRepository productRepository;
    public OrderService(OrderRepository orderRepository,
                        ModelMapper modelMapper,
                        UsertableRepository usertableRepository,
                        OrderProductRepository orderProductRepository,
                        ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.modelMapper = modelMapper;
        this.usertableRepository = usertableRepository;
        this.orderProductRepository = orderProductRepository;
        this.productRepository = productRepository;
    }

    @Transactional
    public void makeOrder(OrderDTO order){
        UsertableEntity user = usertableRepository.findByUsername(order.
                getUser().getUsername());
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setDeliveryStatus("Доставляется");
        orderEntity.setPaymentStatus("Оплачено");
        orderEntity.setUser(user);
        orderEntity.setCost(order.getCost());
        OrderEntity savedOrder = orderRepository.save(orderEntity);
        List<OrderProductEntity> orderProductEntityList =
                new ArrayList<>();
        AtomicInteger i = new AtomicInteger();
        order.getProducts().stream().map(product ->
                product.getProductid()).forEach(id ->
        {
            ProductEntity productEntity =
                    productRepository.findById(id).get();
            OrderProductEntity orderProductEntity =
                    new OrderProductEntity();
            orderProductEntity.setOrder(savedOrder);
            orderProductEntity.setQuantity(order.getProducts()
                    .get(i.get()).getQuantity());
            i.set(i.get() + 1);
            orderProductEntity.setProduct(productEntity);
            orderProductEntityList.add(orderProductEntity);
        });
        List<OrderProductEntity> orderProductEntities =
                orderProductRepository.saveAll(orderProductEntityList);
    }

    @Transactional
    public List<OrderDTO> getAllOrdersByUser(AccountDTO account){
        //Получаю пользователя
        UsertableEntity user = usertableRepository.
                findByUsername(account.getUsername());
        //Получаю все заказы у пользователя
        List<OrderEntity> orders =
                orderRepository.findByUser(user);

        List<OrderDTO> ordersDTO = new ArrayList<>();
        orders.forEach(order ->
        {
            OrderDTO orderDTO = new OrderDTO();
            List<ProductDTO> productsDTO =
                    new ArrayList<>();
            orderProductRepository.findByOrder(order)
                    .forEach(orderProduct ->
                    {
                        log.debug("SDFSDFSFDSfFSDFD" +
                                orderProduct.getQuantity());
                        productsDTO.add(new ProductDTO(
                                orderProduct.getProduct().getProductid(),
                                orderProduct.getProduct().getName(),
                                orderProduct.getProduct().getPrice(),
                                orderProduct.getProduct().getCategories()
                                .stream().map(category ->
                                        modelMapper.map(category,
                                                CategoryDTO.class))
                                .collect(Collectors.toList()),
                                orderProduct.getQuantity()
                        ));
                    });
            orderDTO.setOrderid(order.getOrderid());
            orderDTO.setProducts(productsDTO);
            orderDTO.setCost(order.getCost());
            orderDTO.setDeliveryStatus(order.getDeliveryStatus());
            orderDTO.setPaymentStatus(order.getPaymentStatus());
            orderDTO.setUser(account);
            ordersDTO.add(orderDTO);
        });
        return ordersDTO;
    }
}
