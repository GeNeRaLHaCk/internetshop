package ru.onlineshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import ru.onlineshop.dto.AccountDTO;
import ru.onlineshop.dto.LogInDTO;
import ru.onlineshop.dto.RegisterDTO;

@Service
public class AuthenticationService implements UserAuthenticationService {
    public final UserService userService;
    public final AuthenticationManager authManager;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public AuthenticationService(AuthenticationManager authManager, BCryptPasswordEncoder passwordEncoder, UserService userService) {
        this.authManager = authManager;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    //todo очень много переносов на другую строку в методе, на глаз тяжело воспринимается.
    // если на экран на помещается (+-), то переносим. или если стримы, там каждая новая функция на новую строку
    // почему @RequestBody перед параметром?
    @Override
    public Authentication authorize(@RequestBody
                                        @Validated LogInDTO loginDto) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(
                        loginDto.getLogin(),
                        loginDto.getPassword());
        //todo лучше логировать, а не через System.out. можно просто через ломбок @Slf4j получить log
        // и зачем в логи выводить дто, с паролями) даже если зашифрованные
        System.out.println("authorize,lOgInDto:: "+ loginDto);
        Authentication authentication =
                authManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().
                setAuthentication(authentication);
        return authentication;
    }

    //@Secured("ROLE_ADMIN")
    @Override
    public AccountDTO registerCustomer(RegisterDTO registerDto) {
        //todo здесь легче сразу вернуть userService.saveCustomer(registerDto), незачем объявлять переменную
        AccountDTO accountDTO = userService.saveCustomer(registerDto);
        return accountDTO;
    }

    @Override
    public void logout() {
        SecurityContextHolder.clearContext();
    }

/*    @Override
    public Optional<UserEntity> findByToken(String token) {
        UserEntity userEntity = userService.
                findUserByUsernameAndPassword("test", "123");
*//*        UserEntity user = new UserEntity();
        usertable.setUserid(usertable.getUserid());
        usertable.setUsername(usertable.getUsername());
        usertable.setPassword(usertable.getPassword());*//*
        return Optional.of(userEntity);
        //userService.findToken(token);
    }*/

}