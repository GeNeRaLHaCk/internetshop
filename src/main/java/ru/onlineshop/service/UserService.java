package ru.onlineshop.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.onlineshop.dto.AccountDTO;
import ru.onlineshop.dto.RegisterDTO;
import ru.onlineshop.entity.RoleEntity;
import ru.onlineshop.entity.UsertableEntity;
import ru.onlineshop.exceptions.InvalidDateException;
import ru.onlineshop.exceptions.UserAlreadyExistException;
import ru.onlineshop.exceptions.UserNotFoundException;
import ru.onlineshop.repository.UsertableRepository;
import ru.onlineshop.security.Principal;

import javax.transaction.Transactional;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {
    private final UsertableRepository usertableRepository;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder bcrypt;
    @Autowired
    public UserService(UsertableRepository usertableRepository,
                       ModelMapper modelMapper,
                       BCryptPasswordEncoder bcrypt) {
        this.usertableRepository = usertableRepository;
        this.modelMapper = modelMapper;
        this.bcrypt = bcrypt;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        System.out.println("UsRnM: "+ username);
        UsertableEntity user =
                usertableRepository.findByUsername(username);
        if(user == null) throw new UsernameNotFoundException(
                String.format("User %s not found", username));
        System.out.println("User which we get: " + user.getUsername());

        //todo такие большие конструкторы не приветсвуются.
        // либо отдельный метод, который возвращает Principal, либо паттерном Builder. в ломбоке есть аннотация
        return new Principal(
                user.getUsername(),
                user.getPassword(),
                user.getFirst_name(),
                user.getLast_name(),
                user.getMiddle_name(),
                user.getDate_birthday(),
                user.isEnable(),
                new ArrayList<>(user.getRoles()));
/*        Collections.singleton((GrantedAuthority)
                new SimpleGrantedAuthority("ROLE_CUSTOMER"))*/
    }
    @Transactional
    public Iterable<AccountDTO> getAllUsers(){
        List<UsertableEntity> users = usertableRepository.findAll();
        List<AccountDTO> accounts = new ArrayList<>();
        users.forEach(user
                -> {
            if(!user.getUsername().equals("admin")) {
                accounts.add(
                        modelMapper.map(
                                user, AccountDTO.class));
            }
        });
        return accounts;
    }
    @Transactional
    public AccountDTO getUserById(Long id){
        UsertableEntity user =
                usertableRepository.findById(id)
                        .orElseThrow(()
                                -> new UserNotFoundException());
        return modelMapper.map(user, AccountDTO.class);
    }
    @Transactional
    public AccountDTO deactivateUser(Long id){
        UsertableEntity user =
                usertableRepository.findById(id)
                        .orElseThrow(()
                                -> new UserNotFoundException());
        user.setEnable(false);
        UsertableEntity updatedUser = usertableRepository.save(user);
        AccountDTO account = modelMapper.map(
                updatedUser, AccountDTO.class);
        return account;
    }
    @Transactional
    public AccountDTO activateUser(Long id){
        UsertableEntity user =
                usertableRepository.findById(id)
                        .orElseThrow(()
                                -> new UserNotFoundException());
        user.setEnable(true);
        UsertableEntity updatedUser = usertableRepository.save(user);
        AccountDTO account = modelMapper.map(
                updatedUser, AccountDTO.class);
        return account;
    }
    @Transactional
    public AccountDTO deleteById(Long id){
        UsertableEntity user =
                usertableRepository.findById(id)
                        .orElseThrow(()
                                -> new UserNotFoundException());
        usertableRepository.deleteById(id);
        AccountDTO account = modelMapper.map(user, AccountDTO.class);
        return account;
    }
    @Transactional
    public AccountDTO saveCustomer(RegisterDTO user) {
        if(user.getDate_birthday() == null) throw new InvalidDateException();
        if(usertableRepository.findByUsername(user.getLogin()) != null)
            throw new UserAlreadyExistException();
        UsertableEntity usertableEntity =
                new UsertableEntity();
        usertableEntity.setUsername(user.getLogin());
        usertableEntity.setPassword(bcrypt.encode(user.getPassword()));
        usertableEntity.setFirst_name(user.getFirst_name());
        usertableEntity.setLast_name(user.getLast_name());
        usertableEntity.setMiddle_name(user.getMiddle_name());
        usertableEntity.setDate_birthday(Date.valueOf(user.getDate_birthday()));
        usertableEntity.setRoles(Collections.singleton(
                new RoleEntity(1L, "ROLE_CUSTOMER")));
        usertableEntity.setEnable(true);
        UsertableEntity addedUser = usertableRepository.save(usertableEntity);
        AccountDTO account = modelMapper.map(addedUser, AccountDTO.class);
        return account;
    }
    private Collection<? extends GrantedAuthority>
    mapRolesToAuthorities(Collection<RoleEntity> roles){
        return roles.stream().map(
                r -> new SimpleGrantedAuthority(r.getName()))
                .collect(Collectors.toList());
    }
}
