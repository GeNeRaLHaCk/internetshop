package ru.onlineshop.service;

import org.springframework.security.core.Authentication;
import ru.onlineshop.dto.AccountDTO;
import ru.onlineshop.dto.LogInDTO;
import ru.onlineshop.dto.RegisterDTO;

public interface UserAuthenticationService {
    Authentication authorize(LogInDTO loginDto);
    AccountDTO registerCustomer(RegisterDTO registerDto);
    void logout();
}
