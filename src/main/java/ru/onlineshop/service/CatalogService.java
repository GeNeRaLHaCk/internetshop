package ru.onlineshop.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import ru.onlineshop.dto.CategoryDTO;
import ru.onlineshop.dto.ProductDTO;
import ru.onlineshop.entity.CategoryEntity;
import ru.onlineshop.entity.ProductEntity;
import ru.onlineshop.exceptions.ProductsNotFoundException;
import ru.onlineshop.repository.CategoryRepository;
import ru.onlineshop.repository.ProductRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CatalogService {
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final ModelMapper modelMapper;
    public CatalogService(CategoryRepository categoryRepository, ProductRepository productRepository, ModelMapper modelMapper) {
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
        this.modelMapper = modelMapper;
    }

    @Transactional
    public Iterable<ProductDTO> getAllProducts(){
        List<ProductEntity> products = productRepository.findAll();
        if(products == null) throw new ProductsNotFoundException();
        //todo в стримах вот так лучше строки переносить. здесь еще можно сразу вернуть лист.
        // еще лучше более релевантно называть, если лист, то products, во множественном числе
        List<ProductDTO> catalog = products.stream()
                .map(x -> modelMapper.map(x, ProductDTO.class))
                .collect(Collectors.toList());
        return catalog;
    }
    @Transactional
    public ProductDTO getProductById(Long id){
        ProductEntity product = productRepository.findById(id)
                .orElseThrow(() -> new ProductsNotFoundException());
        ProductDTO productDto = modelMapper.map(product, ProductDTO.class);
        return productDto;
    }
    @Transactional
    public ProductDTO getProductsByCategoryId(Long id){
        return null;
    }
    @Transactional
    public List<CategoryDTO> getAllCategories(){
        List<CategoryEntity> categories = categoryRepository.findAll();
        return categories.stream().map(
                x -> modelMapper.map(x, CategoryDTO.class))
                .collect(Collectors.toList());
    }
}
