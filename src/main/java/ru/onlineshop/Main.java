package ru.onlineshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.onlineshop.config.SpringConfig;

import java.text.DecimalFormat;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(SpringConfig.class, args);
    }
}
