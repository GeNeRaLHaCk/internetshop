import VeeValidate from "vee-validate";

VeeValidate.Validator.prototype.pureValidate = function (name, value, scope) {
  if (scope === void 0) scope = null;

  if (this.paused) {
    return Promise.resolve(true);
  }

  // overload to validate all.
  if (arguments.length === 0) {
    return this.validateScopes();
  }

  // overload to validate scope-less fields.
  if (arguments.length === 1 && arguments[0] === '*') {
    return this.validateAll();
  }

  // overload to validate a scope.
  if (arguments.length === 1 && typeof arguments[0] === 'string' && /^(.+)\.\*$/.test(arguments[0])) {
    var matched = arguments[0].match(/^(.+)\.\*$/)[1];
    return this.validateAll(matched);
  }

  var field = this._resolveField(name, scope);
  if (!field) {
    return this._handleFieldNotFound(name, scope);
  }

  field.flags.pending = true;
  if (arguments.length === 1) {
    value = field.value;
  }

  return this._validate(field, value, true).then(function (result) {
    field.setFlags({
      pending: false,
      validated: false
    });

    return result.valid;
  });
};

VeeValidate.Validator.prototype.pureValidateAll = function (values) {
  var arguments$1 = arguments;
  var this$1 = this;

  if (this.paused) {
    return Promise.resolve(true);
  }

  var matcher = null;
  var providedValues = false;

  if (typeof values === 'string') {
    matcher = {scope: values};
  } else if (isObject(values)) {
    matcher = Object.keys(values).map(function (key) {
      return {name: key, scope: arguments$1[1] || null};
    });
    providedValues = true;
  } else if (arguments.length === 0) {
    matcher = {scope: null}; // global scope.
  } else if (Array.isArray(values)) {
    matcher = values.map(function (key) {
      return {name: key, scope: arguments$1[1] || null};
    });
  }

  var promises = this.fields.filter(matcher).map(function (field) {
    return this$1.pureValidate(
      ("#" + (field.id)),
      providedValues ? values[field.name] : field.value
    );
  });

  return Promise.all(promises).then(function (results) {
    return results.every(function (t) {
      return t;
    });
  });
};

VeeValidate.Validator.prototype.resolveFieldsScope = function () {
  this.fields.items.forEach(function (field) {
    if (field.el !== null) {
      if (!field.scope) {
        let parentScopeNode = field.el.closest('[data-vv-scope]');
        if (parentScopeNode) {
          let parentScope = parentScopeNode.getAttribute('data-vv-scope');
          field.scope = parentScope;
        }
      }
    }
  });
};
