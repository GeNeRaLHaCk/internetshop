import moment from "moment";

export default {
  currentDate() {
    return moment(new Date()).format('YYYY-MM-DD');
  }
}
