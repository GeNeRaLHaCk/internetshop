export default {
  showMessage (message, type) {
    var toastr = this.toastrFull();
    var toastrElement = toastr.Add({
      msg: this.completeMessage(message),
      clickClose: false,
      type: type,
      timeout: (type == 'error') ? 0 : 5000,
      onClicked() {
        if (event && event.target && event.target.className.includes('toast-close-button')) {
          toastr.Close(toastrElement);
        }
      }
    });
  },
  showWMessage (message) {
    this.showMessage(message, 'warning');
  },
}
