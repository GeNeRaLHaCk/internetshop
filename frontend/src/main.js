import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueRouter from 'vue-router'
import {i, a} from './learn.js'
import router from './router/index.js' //ekzeplyar
import store from './store'
import 'material-design-icons-iconfont'
import './assets/styles/styles.scss'
import VeeValidate, {Validator} from 'vee-validate';
import ru from "./assets/js/vee-validate/locale/ru";
import Vuetify from "vuetify";
import qs from 'qs';
import 'vuetify/dist/vuetify.min.css';

axios.defaults.paramsSerializer = params => {
  return qs.stringify(params, { arrayFormat: 'brackets' });
};
Vue.prototype.axios = axios
Vue.use(VueRouter)
Validator.localize('ru', ru);
Vue.use(VeeValidate, {
  inject: false
});
Vue.use(Vuetify, {
  theme: {
    primary: '#154345',
    accent: '#154345',
    error: '#951a1d'
  },
  icons: {
    iconfont: 'mdi'
  }
});
Vue.config.productionTip = false

console.log(`Peremennaya: ${i+a}`);
new Vue({
  el:'#app',
  render: h => h(App),
  store,
  //ykazivaem v kachestve argumenta pri sozdanii ekzemplyara vue
  router,
  Vuetify
}).$mount('#app')
