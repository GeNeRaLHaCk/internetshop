export default function formattedPrice(value) {
    let parts = value.toString().split(".");
    //мы ставим после первой цифры, после тысячной пробел или точку
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g," ");
    return parts.join(".")
}