 import VueRouter from "vue-router"
 import Vue from 'vue'
 import Authorization from "@/components/authentication/Authorization";
 import Registration from "@/components/authentication/Registration";
 import Catalog from "@/components/catalog/Catalog";
 import Cart from "../components/cart/Cart"
 import MainPage from "@/components/main-page/MainPage";
 import AdminPanel from "@/components/profile/admin/AdminPanel";
 import CreateOrder from "@/components/cart/CreateOrder";
 import UserPanel from "@/components/profile/user/UserPanel";
 Vue.use(VueRouter);

let router = new VueRouter({
    mode: 'history',
    routes: [
        {
          path: '/main',
          name: 'main',
          component: MainPage
        },
        {
            path: '/catalog',
            name: 'catalog',
            component: Catalog,
            meta: {
              isRoot: true
            }
/*            beforeRouteLeave: checkAuth()*/
        },
        {
            path: '/catalog/cart',
            name: 'cart',
            component: Cart,
            props: true //ecli nam nado peredatb kakie-libo poropsbI
        },
        {
            path: '/',
            redirect: '/login'
        },
        {
            name: 'authorization',
            path: '/login',
            component: Authorization,
        },
        {
            name: 'registration',
            path: '/register',
            component: Registration,
        },
        {
            name: 'admin',
            path: '/admin',
            component: AdminPanel,
        },
        {
            name: 'user',
            path: '/user',
            component: UserPanel
        },
        {
          name: 'order',
          path: '/order',
          component: CreateOrder,
          props: true
        },
    ]
});

export default router;
