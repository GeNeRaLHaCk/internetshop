import axios from "axios";
import router from "@/router";
import store from "@/store";
export default {
    GET_PRODUCTS_FROM_API({commit}){
/*        axios.get('http://localhost:8090/login/admin/123456')
            .then((result) => {
                console.log('RESULT AUTHORIZATION:', result);
            });*/
        return axios('http://localhost:8090/catalog/list', {
            method: "GET", //TYT BUDET OTVET PROMISE
        })
            .then((products) => {
                console.log('RESULT PRODUCTS:', products.data)
                commit('SET_PRODUCTS_TO_STATE', products.data);//vizivaem mutation CHTOBI polojit dannie v state
                return products;
            })
            .catch((error) => {
                console.log('EXCEPTION GET DATA:',error);
                return error;
            })
    },
    REGISTER_USER({commit}, registrationObject){
        console.log("REGISTRATION_OBJ: ", registrationObject)
        return axios.
        post('http://localhost:8090/register',
            registrationObject)
            .then((result) => {
                console.log('fsdpoasdfsdfoa',result.data);
                commit('SET_USER', registrationObject)
                router.push('/login');
        })
            .catch((error) => {
                if (error.response) {
                    var keys = [];
                    for (var key in error.response.data) {
                        keys.push(key);
                    }
                    console.log("keys of exception:")
                    console.log(keys)
                    console.log("EXCEPTION REGISTER" + error.response.data.error);
                    //window.location.reload();
                }
            });
    },
    AUTH({commit}, authObj){
      return axios({
        method:'get',
        url: 'http://localhost:8090/login/'
          + authObj.login + '/'
          + authObj.password,
        data: `username=${ authObj.login }&password=${  authObj.password }`
      })
    },
    AUTHORIZE({commit}, authorizationObject){
        let url = 'http://localhost:8090/login/'
            + authorizationObject.login + '/'
            + authorizationObject.password;
        return axios(url, {
            method: "GET", //TYT BUDET OTVET PROMISE
        })
            .then((result) => {
                commit('SET_USER', result.data);
                console.log('SUCCESSFUL_AUTH!')
                //vizivaem mutation CHTOBI polojit dannie v state
                //router.push("/catalog")
                commit('SET_AUTH', true);
                if(authorizationObject.username == "admin"){
                  commit('SET_ADMIN', true);
                }
              router.push('/catalog');
                return result.data;
            })
            .catch((exception) => {
                var keys = [];
                for (var key in exception.response.data) {
                    keys.push(key);
                }
                console.log("keys of exception:")
                console.log(keys)
                console.log("EXCEPTION LOG IN: "
                    + exception.response.data.error)
            });
    },
    GET_USERS_FROM_API({commit}) {
/*        if(store.dispatch().isAuth === true && store.isAdmin === true) {*/
            return axios.get('http://localhost:8090/users/list')
                .then((response) => {
                    console.log('RESPONSE ANSWER: ', response.data);
                commit('SET_USERS', response.data)
            }).catch((response) => {
                console.log('RESPONSE GET USERS EXCEPTION: ', response.data);
            })
/*        }*/
    },
    MAKE_ORDER({commit}, order){
      console.log("MAKE_ORDER_PRODUCTS:", order.products)
      console.log("SFDDD", order.user.username)
      return axios({
        method:'post',
        url: 'http://localhost:8090/user/buy',
        data: {
          user: {
              username: order.user.username
            },
          products: order.products,
          cost: order.cost
        }
      })
        .then((result) => {
          commit('CLEAR_CART');
        })
    },
    GET_ALL_CATEGORIES(){
      return axios({
        method: 'get',
        url: 'http://localhost:8090/catalog/categories'
      });
    },
    GET_ALL_ORDERS_BY_USER({commit}, user){
      console.log("FDSSSSSSSSSSSSSSSSSSSSSSS", );
      return axios.post('http://localhost:8090/user/orders', {
        user: user
      })
        .then((result) => (
        console.log("GET_ALL_ORDERS_BY_USERFF", result.data)
      )).catch((ex) => {
        alert(ex)
      });
    }
}
