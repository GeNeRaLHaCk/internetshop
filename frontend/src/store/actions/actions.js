
export default {
    ADD_TO_CART({commit}, product)
    {//commit chtobi vizivat mutation
        commit('SET_CART', product);
    }
,
    DELETE_FROM_CART({commit}, product)
    {
        commit('REMOVE_FROM_CART', product);
    }
,
    INCREMENT_CART_ITEM({commit}, i)
    {
        commit('INCREMENT', i);
    }
,
    DECREMENT_CART_ITEM({commit}, i)
    {
        commit('DECREMENT', i);
    },
    SET_SEARCH_VALUE_TO_VUEX({commit}, value){
        commit('SET_SEARCH_VALUE_TO_VUEX', value);
    },
    SET_USER({commit}, user){
      commit('SET_USER', user);
    }
}
