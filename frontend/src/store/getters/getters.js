export default {
    SEARCH_VALUE(state){
      return state.searchValue;
    },
    PRODUCTS(state){
        return state.products;
    },
    CARTS(state){
        console.log('CARTSGETTER:', state.carts)
        return state.carts;
    },
    USER(state){
        return state.user;
    },
    USERS(state){
        return state.users;
    },
    ISAUTH(state) {
        return state.isAuth;
    },
    ISADMIN(state) {
        return state.isAdmin
    }
}