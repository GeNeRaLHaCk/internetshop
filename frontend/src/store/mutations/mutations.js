export default {
    SET_PRODUCTS_TO_STATE: function (state, products) {
        state.products = products
    }, // POPITKA IZMENENIYA STATE BEZ ACTIONS OR MUTATIONS ZAPRETITELNO!
    SET_CART: function (state, product) {
        console.log('SET_CART, STATE:', state);
        if(state.carts.length){ //esli korzina estb, ya begu po objectam i smotruy sootvetstvuet li id
            //dobavlyaemogo i dobavlennih i esli sootvetstvuet=> ++quantity, inache dobavlyaem
            let isProductExist = false;
            state.carts.map(function (item) {
                if (item.productid === product.productid){
                    isProductExist = true;
                    item.quantity++;
                }
            })
            if(!isProductExist) {
                state.carts.push(product)
            }
        } else { //esli korzini nety, ya kladu tovar
            state.carts.push(product)
        }
    }, //budet v nash state cart pushit nash novii product
    REMOVE_FROM_CART: function (state, index){
        state.carts.splice(index,  1)
    },
    INCREMENT: function (state, index){
        state.carts[index].quantity++;
    },
    DECREMENT: function (state, index){
        if(state.carts[index].quantity > 1){
            state.carts[index].quantity--;
        }
    },
    SET_USER: function (state, user){
        state.user = user;
    },
    SET_USERS: function (state, users){
        state.users = users;
    },
    SET_AUTH: function (state, bool){
        state.isAuth = bool;
    },
    SET_SEARCH_VALUE_TO_VUEX: (state, value) => {
        state.searchValue = value;
    },
    CLEAR_CART: (state) => {
      state.carts = [];
    }
}
