import Vue from 'vue'
import Vuex from 'vuex'

import commonActions from "@/store/actions/actions";
import apiRequest from "@/store/actions/api-request"
import getters from "@/store/getters/getters";
import mutations from "@/store/mutations/mutations";

const actions = {...commonActions, ...apiRequest}

Vue.use(Vuex)

let store = new Vuex.Store({ //Store globalnaya bd nashego prilojeniya
  state: {// sostoyanie, objects, massives, collections
    products: [],
    carts: [],
    isAuth: false,
    users: [],
    isAdmin: true,
    user: Object,
    searchValue: ''
  },
  mutations, //c pomoshbyu this, menyaem sostoyanie v state, sinhronni
  actions, //assinhronno
  getters,
  modules: {
  }
});
export default store;
